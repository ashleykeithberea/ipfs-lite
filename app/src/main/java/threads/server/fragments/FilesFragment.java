package threads.server.fragments;


import android.app.Activity;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;
import androidx.work.WorkManager;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.navigationrail.NavigationRailView;
import com.google.android.material.textview.MaterialTextView;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Network;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.core.files.Proxy;
import threads.server.model.LiteViewModel;
import threads.server.model.SelectionViewModel;
import threads.server.provider.FileProvider;
import threads.server.services.LiteService;
import threads.server.services.MimeTypeService;
import threads.server.services.QRCodeService;
import threads.server.utils.FileItemDetailsLookup;
import threads.server.utils.FilesItemKeyProvider;
import threads.server.utils.FilesViewAdapter;
import threads.server.work.CopyDirectoryWorker;
import threads.server.work.CopyFileWorker;
import threads.server.work.PageRefreshWorker;
import threads.server.work.PublishContentWorker;
import threads.server.work.UploadFolderWorker;


public class FilesFragment extends Fragment implements AccessFragment,
        SwipeRefreshLayout.OnRefreshListener, FilesViewAdapter.FilesAdapterListener {

    private static final String TAG = FilesFragment.class.getSimpleName();
    private final ArrayDeque<Long> stack = new ArrayDeque<>();
    private boolean widthMode = false;
    private long lastClickTime = 0;
    private SelectionViewModel selectionViewModel;
    private FilesViewAdapter filesViewAdapter;
    private RecyclerView recyclerView;
    private ActionMode actionMode;
    private SelectionTracker<Long> selectionTracker;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FileItemDetailsLookup fileItemDetailsLookup;
    private NavigationRailView navigationRailView;
    private ExtendedFloatingActionButton extendedFloatingActionButton;
    private LiteViewModel liteViewModel;
    private final ActivityResultLauncher<Intent> mFolderImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);

                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    int items = mClipData.getItemCount();
                                    if (items > 0) {
                                        for (int i = 0; i < items; i++) {
                                            ClipData.Item item = mClipData.getItemAt(i);
                                            Uri uri = item.getUri();

                                            if (!FileProvider.hasReadPermission(requireContext(), uri)) {
                                                EVENTS.getInstance(requireContext()).error(
                                                        getString(R.string.file_has_no_read_permission));
                                                return;
                                            }

                                            if (FileProvider.isPartial(requireContext(), uri)) {
                                                EVENTS.getInstance(requireContext()).error(
                                                        getString(R.string.file_not_valid));
                                                return;
                                            }

                                            UploadFolderWorker.load(requireContext(),
                                                    getParentProxy(), uri);
                                        }
                                    }
                                } else {
                                    Uri uri = data.getData();
                                    if (uri != null) {
                                        if (!FileProvider.hasReadPermission(requireContext(), uri)) {
                                            EVENTS.getInstance(requireContext()).error(
                                                    getString(R.string.file_has_no_read_permission));
                                            return;
                                        }

                                        if (FileProvider.isPartial(requireContext(), uri)) {
                                            EVENTS.getInstance(requireContext()).error(
                                                    getString(R.string.file_not_valid));
                                            return;
                                        }

                                        UploadFolderWorker.load(requireContext(),
                                                getParentProxy(), uri);
                                    }
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mFilesImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);

                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();

                                    LiteService.files(requireContext(), mClipData,
                                            getParentProxy());

                                } else if (data.getData() != null) {
                                    Uri uri = data.getData();
                                    Objects.requireNonNull(uri);
                                    if (!FileProvider.hasReadPermission(requireContext(), uri)) {
                                        EVENTS.getInstance(requireContext()).error(
                                                getString(R.string.file_has_no_read_permission));
                                        return;
                                    }

                                    if (FileProvider.isPartial(requireContext(), uri)) {
                                        EVENTS.getInstance(requireContext()).error(
                                                getString(R.string.file_not_valid));
                                        return;
                                    }

                                    LiteService.file(requireContext(), getParentProxy(), uri);
                                }

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mDirExportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                Uri uri = data.getData();
                                Objects.requireNonNull(uri);

                                if (!FileProvider.hasWritePermission(requireContext(), uri)) {
                                    EVENTS.getInstance(requireContext()).error(
                                            getString(R.string.file_has_no_write_permission));
                                    return;
                                }

                                CopyDirectoryWorker.copyTo(requireContext(), uri, getParentProxy());

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mFileExportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                Uri uri = data.getData();
                                Objects.requireNonNull(uri);
                                if (!FileProvider.hasWritePermission(requireContext(), uri)) {
                                    EVENTS.getInstance(requireContext()).error(
                                            getString(R.string.file_has_no_write_permission));
                                    return;
                                }

                                CopyFileWorker.copyTo(requireContext(), uri, getParentProxy());

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        releaseActionMode();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (selectionTracker != null) {
            selectionTracker.onSaveInstanceState(outState);
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void findInPage() {
        try {
            if (isResumed()) {
                actionMode = ((AppCompatActivity)
                        requireActivity()).startSupportActionMode(
                        createSearchActionModeCallback());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.files_view, container, false);
    }

    private void switchDisplayModes() {
        if (!widthMode) {
            navigationRailView.setVisibility(View.GONE);
        } else {
            navigationRailView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            evaluateDisplayModes();
            switchDisplayModes();
            showFab(Objects.requireNonNull(selectionViewModel.getShowFab().getValue()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void clickFilesAdd() {

        try {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType(MimeTypeService.ALL);
            String[] mimeTypes = {MimeTypeService.ALL};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            mFilesImportForResult.launch(intent);

        } catch (Throwable throwable) {
            EVENTS.getInstance(requireContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
            LogUtils.error(TAG, throwable);
        }
    }

    private void showFab(boolean showFab) {
        if (widthMode) {
            extendedFloatingActionButton.hide();
        } else {
            if (showFab) {
                extendedFloatingActionButton.show();
            } else {
                extendedFloatingActionButton.hide();
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        extendedFloatingActionButton = view.findViewById(R.id.floating_action_button);


        extendedFloatingActionButton.setOnClickListener((v) -> {

            if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            clickFilesAdd();

        });


        navigationRailView = view.findViewById(R.id.navigation_rail);
        switchDisplayModes();


        navigationRailView.setOnItemSelectedListener(item -> {
            int id = item.getItemId();
            if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            if (id == R.id.menu_add_file) {
                try {
                    clickFilesAdd();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_new_text) {
                try {


                    TextDialogFragment.newInstance(liteViewModel.getParentProxyValue()).
                            show(getChildFragmentManager(), TextDialogFragment.TAG);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_import_folder) {
                try {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                    mFolderImportForResult.launch(intent);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_new_folder) {
                try {
                    NewFolderDialogFragment.newInstance(liteViewModel.getParentProxyValue()).
                            show(getChildFragmentManager(), NewFolderDialogFragment.TAG);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            }
            return false;
        });


        MaterialTextView textHomepageUriTitle = view.findViewById(R.id.text_homepage_uri_title);
        textHomepageUriTitle.setText(getString(R.string.homepage));
        MaterialTextView textHomepageUri = view.findViewById(R.id.text_homepage_uri);

        try {
            DOCS docs = DOCS.getInstance(requireContext());
            textHomepageUri.setText(docs.getHomePageUri().toString());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        selectionViewModel = new ViewModelProvider(requireActivity()).get(SelectionViewModel.class);

        selectionViewModel.getShowFab().observe(getViewLifecycleOwner(), (showFab) -> {
            try {
                if (showFab != null) {
                    showFab(showFab);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        liteViewModel = new ViewModelProvider(requireActivity()).get(LiteViewModel.class);

        recyclerView = view.findViewById(R.id.recycler_view_message_list);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setItemAnimator(null);

        filesViewAdapter = new FilesViewAdapter(requireContext(), this);
        recyclerView.setAdapter(filesViewAdapter);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                boolean hasSelection = selectionTracker.hasSelection();
                if (dy > 0) {
                    selectionViewModel.setShowFab(false);
                } else if (dy < 0 && !hasSelection) {
                    selectionViewModel.setShowFab(true);
                }

                swipeRefreshLayout.setEnabled
                        (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0);
            }
        });

        fileItemDetailsLookup = new FileItemDetailsLookup(recyclerView);


        swipeRefreshLayout = view.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);


        selectionTracker = new SelectionTracker.Builder<>(TAG, recyclerView,
                new FilesItemKeyProvider(filesViewAdapter),
                fileItemDetailsLookup,
                StorageStrategy.createLongStorage())
                .build();


        selectionTracker.addObserver(new SelectionTracker.SelectionObserver<>() {
            @Override
            public void onSelectionChanged() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle("" + selectionTracker.getSelection().size());
                }
                super.onSelectionChanged();
            }

            @Override
            public void onSelectionRestored() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle("" + selectionTracker.getSelection().size());
                }
                super.onSelectionRestored();
            }
        });

        filesViewAdapter.setSelectionTracker(selectionTracker);


        liteViewModel.getLiveDataFiles().observe(getViewLifecycleOwner(), (proxies) -> {
            if (proxies != null) {
                try {
                    int size = filesViewAdapter.getItemCount();
                    boolean scrollToTop = size < proxies.size();

                    filesViewAdapter.updateData(proxies);

                    if (scrollToTop) {
                        try {
                            recyclerView.scrollToPosition(0);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        });


        if (savedInstanceState != null) {
            selectionTracker.onRestoreInstanceState(savedInstanceState);
        } else {
            evaluateDisplayModes();
            switchDisplayModes();
        }

    }


    private long[] convert(Selection<Long> entries) {
        int i = 0;

        long[] basic = new long[entries.size()];
        for (Long entry : entries) {
            basic[i] = entry;
            i++;
        }

        return basic;
    }

    private void deleteAction() {

        final EVENTS events = EVENTS.getInstance(requireContext());

        if (!selectionTracker.hasSelection()) {
            events.warning(getString(R.string.no_marked_file_delete));
            return;
        }


        try {
            long[] entries = convert(selectionTracker.getSelection());

            selectionTracker.clearSelection();

            removeFiles(entries);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void removeFiles(long... indices) {

        FILES files = FILES.getInstance(requireContext());
        EVENTS events = EVENTS.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            long start = System.currentTimeMillis();

            try {
                files.setFilesDeleting(indices);

                events.delete(Arrays.toString(indices));

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }

        });
    }


    @Override
    public void invokeAction(@NonNull Proxy proxy, @NonNull View view) {

        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();

        try {
            boolean isSeeding = proxy.isSeeding();

            PopupMenu menu = new PopupMenu(requireContext(), view);
            menu.inflate(R.menu.popup_proxy_menu);
            menu.getMenu().findItem(R.id.popup_rename).setVisible(true);
            menu.getMenu().findItem(R.id.popup_share).setVisible(true);
            menu.getMenu().findItem(R.id.popup_delete).setVisible(true);
            menu.getMenu().findItem(R.id.popup_copy_to).setVisible(isSeeding);

            menu.setOnMenuItemClickListener((item) -> {

                if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                if (item.getItemId() == R.id.popup_info) {
                    clickProxyInfo(proxy);
                    return true;
                } else if (item.getItemId() == R.id.popup_delete) {
                    clickProxyDelete(proxy.getIdx());
                    return true;
                } else if (item.getItemId() == R.id.popup_publish) {
                    clickProxyPublish(proxy.getIdx());
                    return true;
                } else if (item.getItemId() == R.id.popup_share) {
                    clickProxyShare(proxy.getIdx());
                    return true;
                } else if (item.getItemId() == R.id.popup_copy_to) {
                    clickProxyCopy(proxy);
                    return true;
                } else if (item.getItemId() == R.id.popup_rename) {
                    clickProxyRename(proxy);
                    return true;
                } else {
                    return false;
                }
            });

            menu.show();


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


    }


    private void clickProxyRename(@NonNull Proxy proxy) {
        try {
            RenameFileDialogFragment.newInstance(proxy.getIdx(), proxy.getName()).
                    show(getChildFragmentManager(), RenameFileDialogFragment.TAG);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void clickProxyShare(long idx) {
        EVENTS events = EVENTS.getInstance(requireContext());
        FILES files = FILES.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            try {
                Proxy proxy = files.getFileByIdx(idx);
                Objects.requireNonNull(proxy);
                ComponentName[] names = {new ComponentName(
                        requireContext().getApplicationContext(), MainActivity.class)};
                Uri uri = DOCS.getInstance(requireContext()).getPath(proxy);

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, uri.toString());
                intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                intent.putExtra(Intent.EXTRA_SUBJECT, proxy.getName());
                intent.putExtra(Intent.EXTRA_TITLE, proxy.getName());


                Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names);
                chooser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(chooser);


            } catch (Throwable ignore) {
                events.warning(getString(R.string.no_activity_found_to_handle_uri));
            }
        });


    }

    private void clickProxyCopy(@NonNull Proxy proxy) {
        try {
            if (proxy.isDir()) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                mDirExportForResult.launch(intent);
            } else {
                Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                intent.setType(proxy.getMimeType());
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                intent.putExtra(Intent.EXTRA_TITLE, proxy.getName());
                intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                mFileExportForResult.launch(intent);
            }
        } catch (Throwable e) {
            EVENTS.getInstance(requireContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
        }
    }

    private ActionMode.Callback createActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_threads_action_mode, menu);

                selectionViewModel.setShowFab(false);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {


                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                int itemId = item.getItemId();
                if (itemId == R.id.action_mode_mark_all) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                        return true;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();

                    filesViewAdapter.selectAll();

                    return true;
                } else if (itemId == R.id.action_mode_delete) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                        return true;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();

                    deleteAction();

                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

                selectionTracker.clearSelection();

                selectionViewModel.setShowFab(true);

                if (actionMode != null) {
                    actionMode = null;
                }

            }
        };

    }

    @Override
    public void onClick(@NonNull Proxy proxy) {

        if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();

        try {
            if (!selectionTracker.hasSelection()) {

                if (actionMode != null) {
                    actionMode.finish();
                    actionMode = null;
                }

                if (proxy.isDir()) {
                    stack.push(proxy.getParent());
                    liteViewModel.setParentProxy(proxy.getIdx());
                } else {
                    clickThreadPlay(proxy);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void clickProxyInfo(@NonNull Proxy proxy) {
        try {
            Cid cid = proxy.getCid();
            Objects.requireNonNull(cid);
            String uri = Content.IPFS + "://" + cid.String();

            Uri uriImage = QRCodeService.getImage(requireContext(), uri);
            ContentDialogFragment.newInstance(uriImage,
                            getString(R.string.url_data_access, proxy.getName()), uri)
                    .show(getChildFragmentManager(), ContentDialogFragment.TAG);


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private void evaluateDisplayModes() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;
        widthMode = widthDp >= 600;
    }

    private void clickThreadPlay(@NonNull Proxy proxy) {

        EVENTS events = EVENTS.getInstance(requireContext());

        if (proxy.isSeeding()) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> {
                try {
                    DOCS docs = DOCS.getInstance(requireContext());
                    Cid cid = proxy.getCid();
                    Objects.requireNonNull(cid);

                    String mimeType = proxy.getMimeType();

                    // special case
                    if (Objects.equals(mimeType, MimeTypeService.URL_MIME_TYPE)) {
                        IPFS ipfs = IPFS.getInstance(requireContext());
                        Uri uri = Uri.parse(ipfs.getText(docs.getSession(), cid, () -> false));
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        return;
                    } else if (Objects.equals(mimeType, MimeTypeService.HTML_MIME_TYPE)) {
                        Uri uri = docs.getIpnsPath(proxy, false);
                        selectionViewModel.setUri(uri);
                        return;
                    }
                    Uri uri = docs.getIpnsPath(proxy, true);
                    //Uri uri = DOCS.getInstance(mContext).getPath(thread);
                    selectionViewModel.setUri(uri);


                } catch (Throwable ignore) {
                    events.warning(getString(R.string.no_activity_found_to_handle_uri));
                }
            });
        }
    }

    @Override
    public void invokePauseAction(@NonNull Proxy proxy) {

        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();


        UUID uuid = proxy.getWorkUUID();
        if (uuid != null) {
            WorkManager.getInstance(requireContext()).cancelWorkById(uuid);
        }

        FILES files = FILES.getInstance(requireContext());
        Executors.newSingleThreadExecutor().submit(() ->
                files.resetThreadLeaching(proxy.getIdx()));

    }


    private int dp48ToPixels() {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) 48 * density);
    }

    private void clickProxyDelete(long idx) {
        removeFiles(idx);
    }


    private void clickProxyPublish(long idx) {

        PublishContentWorker.publish(requireContext(), idx);
        FILES files = FILES.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            long start = System.currentTimeMillis();

            try {
                Proxy proxy = files.getFileByIdx(idx);
                Objects.requireNonNull(proxy);

                Cid cid = proxy.getCid();
                Objects.requireNonNull(cid);

                String url = Settings.GATEWAY + IPFS.IPFS_PATH + cid.String();
                selectionViewModel.setUri(Uri.parse(url));

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }

        });
    }


    private ActionMode.Callback createSearchActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_search_action_mode, menu);

                fileItemDetailsLookup.setActive(false);

                MenuItem searchMenuItem = menu.findItem(R.id.action_search);

                SearchView mSearchView = (SearchView) searchMenuItem.getActionView();

                TextView textView = mSearchView.findViewById(
                        androidx.appcompat.R.id.search_src_text);
                textView.setTextSize(16);
                textView.setMinHeight(dp48ToPixels());

                mSearchView.setIconifiedByDefault(false);
                mSearchView.setFocusable(true);
                mSearchView.setFocusedByDefault(true);
                String query = liteViewModel.getQuery().getValue();
                Objects.requireNonNull(query);
                mSearchView.setQuery(query, true);
                mSearchView.setIconified(false);
                mSearchView.requestFocus();


                mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {

                        liteViewModel.getQuery().setValue(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        liteViewModel.getQuery().setValue(newText);
                        return false;
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                try {
                    fileItemDetailsLookup.setActive(true);
                    liteViewModel.setQuery("");

                    if (actionMode != null) {
                        actionMode = null;
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        };

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);

        try {
            if (!Network.isNetworkConnected(requireContext())) {

                EVENTS.getInstance(requireContext()).warning(
                        getString(R.string.publish_no_network));
            } else {
                EVENTS.getInstance(requireContext()).warning(getString(R.string.publish_files));

                PageRefreshWorker.publish(requireContext());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            swipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void releaseActionMode() {
        try {
            if (isResumed()) {
                if (actionMode != null) {
                    actionMode.finish();
                    actionMode = null;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private long getParentProxy() {
        return liteViewModel.getParentProxyValue();
    }

    public boolean onBackPressed() {
        if (!stack.isEmpty()) {
            liteViewModel.setParentProxy(stack.pop());
            return true;
        }
        return false;
    }
}
