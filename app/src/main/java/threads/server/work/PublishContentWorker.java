package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;
import threads.server.LogUtils;
import threads.server.core.Content;
import threads.server.core.files.FILES;
import threads.server.core.files.Proxy;

public class PublishContentWorker extends Worker {
    private static final String TAG = PublishContentWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public PublishContentWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long idx) {
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(PublishContentWorker.class)
                .setConstraints(constraints)
                .setInputData(data.build())
                .addTag(TAG)
                .build();
    }

    public static void publish(@NonNull Context context, long idx) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, ExistingWorkPolicy.REPLACE, getWork(idx));
    }


    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            FILES files = FILES.getInstance(getApplicationContext());

            Proxy proxy = files.getFileByIdx(idx);
            Objects.requireNonNull(proxy);

            Cid cid = proxy.getCid();
            Objects.requireNonNull(cid);

            try (Session session = ipfs.createSession(false)) {

                Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();
                ipfs.provide(session, cid, providers::add, new TimeoutCancellable(
                        () -> isStopped() || providers.size() > 50
                        , 30));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, "Worker Finish " + getId() +
                    " onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }


}

