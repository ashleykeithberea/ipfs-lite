package threads.server.core.files;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import threads.lite.cid.Cid;

@Dao
public interface ProxyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertProxy(Proxy proxy);

    @Query("SELECT cid FROM Proxy WHERE idx = :idx")
    Cid getContent(long idx);

    @Query("UPDATE Proxy SET leaching = 1 WHERE idx = :idx")
    void setLeaching(long idx);

    @Query("UPDATE Proxy SET leaching = 0 WHERE idx = :idx")
    void resetLeaching(long idx);

    @Query("UPDATE Proxy SET deleting = 1 WHERE idx = :idx")
    void setDeleting(long idx);

    @Query("UPDATE Proxy SET deleting = 0 WHERE idx = :idx")
    void resetDeleting(long idx);

    @Query("SELECT COUNT(idx) FROM Proxy WHERE cid =:cid")
    int references(Cid cid);

    @Query("SELECT * FROM Proxy WHERE idx =:idx")
    Proxy getProxyByIdx(long idx);

    @Query("SELECT * FROM Proxy WHERE parent =:parent AND deleting = 0 AND name LIKE :query ORDER BY lastModified DESC")
    LiveData<List<Proxy>> getLiveDataFiles(long parent, String query);

    @Query("UPDATE Proxy SET cid =:cid, size = :size, lastModified =:lastModified  WHERE idx = :idx")
    void updateContent(long idx, Cid cid, long size, long lastModified);

    @Query("UPDATE Proxy SET name =:name WHERE idx = :idx")
    void setName(long idx, String name);

    @Query("SELECT * FROM Proxy WHERE parent = 0 AND deleting = 0 AND seeding = 1")
    List<Proxy> getPins();

    @Query("SELECT * FROM Proxy WHERE parent =:parent AND deleting = 0")
    List<Proxy> getChildren(long parent);

    @Query("UPDATE Proxy SET seeding = 1, leaching = 0 WHERE idx = :idx")
    void setDone(long idx);

    @Query("UPDATE Proxy SET cid =:cid, seeding = 1, leaching = 0 WHERE idx = :idx")
    void setDone(long idx, Cid cid);

    @Query("UPDATE Proxy SET work = :work WHERE idx = :idx")
    void setWork(long idx, String work);

    @Query("UPDATE Proxy SET work = null WHERE idx = :idx")
    void resetWork(long idx);

    @Query("SELECT * FROM Proxy WHERE deleting = 1 AND lastModified < :time")
    List<Proxy> getDeletedProxies(long time);

    @Delete
    void removeProxy(Proxy proxy);

    @Query("SELECT name FROM Proxy WHERE idx = :idx")
    String getName(long idx);

    @Query("UPDATE Proxy SET uri =:uri WHERE idx = :idx")
    void setUri(long idx, String uri);

}
