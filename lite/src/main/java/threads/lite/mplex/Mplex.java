package threads.lite.mplex;

import android.util.Pair;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;

import threads.lite.LogUtils;
import threads.lite.utils.DataHandler;

public final class Mplex {
    private static final String MUX_ID = "MUX_ID";
    private static final String TAG = Mplex.class.getSimpleName();


    public static MuxId defaultMuxId() {
        return new MuxId(0, true);
    }

    public static MuxId generateMuxId(QuicStream stream) {
        int streamId = stream.getStreamId();
        Integer offset = (Integer) stream.getAttribute(MUX_ID);
        int muxId = offset == null ? 1 : offset;
        muxId++; // increment
        stream.setAttribute(MUX_ID, muxId);

        return new MuxId(streamId + muxId, true);
    }


    /*
     * Encodes the given mplex frame into bytes and writes them into the output.
     * @see [https://github.com/libp2p/specs/tree/master/mplex]
     */
    public static byte[] encode(@NonNull MuxId muxId, @NonNull MuxFlag muxFlag, byte[] data) {
        int header = encodeHeader(muxId, muxFlag);
        int length = data.length;
        int headerLength = DataHandler.unsignedVariantSize(header);
        int lengthLength = DataHandler.unsignedVariantSize(length);
        ByteBuffer out = ByteBuffer.allocate(lengthLength + headerLength + length);
        DataHandler.writeUnsignedVariant(out, header);
        DataHandler.writeUnsignedVariant(out, length);
        out.put(data);
        return out.array();
    }

    public static int encodeHeader(@NonNull MuxId muxId, @NonNull MuxFlag muxFlag) {
        int flag = MplexFlag.toMplexFlag(muxFlag, muxId.isInitiator()).getValue();
        return muxId.getStreamId() << 3 | flag;
    }

    public static Pair<MuxId, MuxFlag> decodeHeader(int header) {
        MplexFlag streamTag = MplexFlag.get(header & 0x07);
        LogUtils.debug(TAG, "Header Tag " + streamTag);
        int streamId = header >> 3;
        LogUtils.debug(TAG, "Header Id " + streamId);
        boolean initiator;
        if (streamTag == MplexFlag.NewStream) {
            initiator = false;
        } else {
            initiator = !MplexFlag.isInitiator(streamTag);
        }
        LogUtils.debug(TAG, "Header initiator " + initiator);

        return Pair.create(new MuxId(streamId, initiator), MplexFlag.toAbstractFlag(streamTag));
    }

}
