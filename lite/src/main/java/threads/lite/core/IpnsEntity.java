package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.Date;

import threads.lite.cid.PeerId;

public class IpnsEntity {

    @NonNull
    private final PeerId peerId;
    private final long sequence;
    private final byte[] data;
    @NonNull
    private final Date eol;

    public IpnsEntity(@NonNull PeerId peerId, @NonNull Date eol, byte[] data, long sequence) {
        this.peerId = peerId;
        this.eol = eol;
        this.sequence = sequence;
        this.data = data;
    }


    @NonNull
    @Override
    public String toString() {
        return "Entry{" +
                "peerId=" + peerId +
                ", sequence=" + sequence +
                ", eol=" + eol +
                '}';
    }

    @NonNull
    public Date getEol() {
        return eol;
    }

    @NonNull
    public PeerId getPeerId() {
        return peerId;
    }

    public long getSequence() {
        return sequence;
    }

    public byte[] getData() {
        return data;
    }
}