package threads.lite.core;


public interface ConnectionHandler {
    void incomingConnection(Connection connection);
}

