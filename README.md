# IPFS Lite
IPFS Lite is an application to support the standard use cases of IPFS


## General 
The basic characteristics of the app are decentralized, respect of personal data,
open source, free of charge, transparent, free of advertising and legally impeccable.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/threads.server/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=threads.server)

## Documentation

**IPFS Lite** based on a subset of IPFS (https://ipfs.io/), which is described in detail
in the **IPFS Lite Library** (https://gitlab.com/remmer.wilts/threads-lite/)

## Privacy Policy

### Data Protection

<p>As an application provider, we take the protection of all personal data very seriously.
All personal information is treated confidentially and in accordance with the legal requirements,
regulations, as explained in this privacy policy.</p>
<p>This app is designed so that the user do not have to enter any personal data. Never will data
collected by us, and especially not passed to third parties. The users behaviour is also not
analyzed by this application.</p>
<p>The user is responsible what kind of data is added or retrieved from the IPFS network.
This kind of information is also not tracked by this application.</p>

### Android Permissions
<p>This section describes briefly why specific Android permissions are required.</p>
            <ul>
                <li>
                    <h4>Camera</h4>
                    <p>The camera permission is required to read QR codes, which contains
                        information about peer ID's (PIDs) or content data (URLs).
                    </p>
                </li>
                <li>
                    <h4>Foreground Service</h4>
                    <p>The foreground service permission is required to run the IPFS node over a
                        longer period of time.
                    </p>
                </li>
            </ul>
